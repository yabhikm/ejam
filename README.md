# eJam Weather App

Code challenge of eJam to build weather application which shows weather forecast of three pre-popluted US cities.
This application use React.js on front side and node.js with express on backend side. A third party API is used to retriew weather information.
React.js is used with typescript and uses hooks for using React features.

## Getting Started
I am assuming that `npm` is already installed on your machine.
To run this application, clone the repository on local machine. You will see two directories(api, ui) on the root. Api directory contains code for server and ui directory contains code for creating UI.
Run `npm install` in each directories to install dependencies respectively.
Once dependencies are installed run `npm start` in each directory to start servers. Once server started successfully you can access UI by accessing url `http://localhost:3000/`.
API can be accessed on port `3001`