const config = require('../config');

const requestForecast = (city) => {
    const https = require('https')
    const options = {
        hostname: 'api.openweathermap.org',
        port: 443,
        path: encodeURI(`/data/2.5/forecast/daily?q=${city}&units=metric&appid=dbb624c32c7f0d652500552c5ebbde56`),
        method: 'GET'
    }
    return new Promise((resolve, reject) => {
        const httpReq = https.request(options, httpRes => {
            let data = '';
            httpRes.on('data', d => {
                data += d;
            });;
    
            httpRes.on('end', () => {
                resolve(JSON.parse(data));
            });
        })
    
        httpReq.on('error', error => {
            reject(error);
        });
    
        httpReq.end();
    });
};

exports.getForecast = (req, res, next) => {
    const { city } = req.params;
    requestForecast(city).then(data => {
        res.status(200).json(data);
    }).catch(err => {
        res.status(200).json({});
    });
};

exports.getForecasts = (req, res, next) => {
    const cities = req.query.q && req.query.q.split(',');
    if (!cities.length) {
        res.status(200).json({});
    }

    const promises = [];

    cities.forEach(city => {
        promises.push(requestForecast(city));
    });

    Promise.all(promises).then(values => {
        const data = {};
        values.forEach((i, index) => {
            data[cities[index]] = i;
        });
        res.status(200).json(data);
    }).catch(() => {
        res.status(200).json({});
    });
};