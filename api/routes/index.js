const express = require('express');
const router = express.Router();

const WeatherController = require('../controllers/WeatherController');

router.get(
    '/forecast/city/:city',
    WeatherController.getForecast
);

router.get(
    '/forecast/cities',
    WeatherController.getForecasts
);

module.exports = router;