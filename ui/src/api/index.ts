import axios from 'axios';

const URL = 'http://localhost:3001/api';

export const getForeCast = (city: string): Promise<any> => {
    return axios.get(`${URL}/forecast/city/${city}`);
};