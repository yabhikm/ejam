import { ThunkDispatch } from 'redux-thunk';
import { getForeCast as getForeCastApi } from '../api';

interface FORECASTLOADING {
    type: 'FORECAST_LOADING'
};

interface FORECASTSUCCESS {
    type: 'FORECAST_SUCCESS',
    payload: Object
};

interface FORECASTFAIL {
    type: 'FORECAST_FAIL'
};

export const forecastLoading = (): FORECASTLOADING => ({
    type: 'FORECAST_LOADING'
});

const forecastSuccess = (payload: Object): FORECASTSUCCESS => ({
    type: 'FORECAST_SUCCESS',
    payload
});

const forecastFail = (): FORECASTFAIL => ({
    type: 'FORECAST_FAIL'
});

export const getForeCast = (city: string) => {
    return (dispatch: ThunkDispatch<any, any, any>) => {
        dispatch(forecastLoading());
        getForeCastApi(city).then(res => {
            const { data: { city = {}, list = [] } } = res || {};
            dispatch(forecastSuccess({city, forecast: list}));
        }).catch(() => {
            dispatch(forecastFail());
        });
    };
};