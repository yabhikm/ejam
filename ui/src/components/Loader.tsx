import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
    Paper,
    CircularProgress,
    Grid,
} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    paperCenter: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    }
}));

const Loader: React.FC = () => {
    const classes = useStyles();
    return (
        <>
            <Grid item xs={1}></Grid>
            <Grid item xs={10}>
                <Paper className={classes.paperCenter}>
                    <CircularProgress />
                </Paper>
            </Grid>
        </>
    );
};

export default Loader