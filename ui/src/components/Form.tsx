import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
    Select,
    MenuItem,
    Button,
    InputLabel,
    Paper,
    Grid,
    FormControl

} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    paperCenter: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    formControl: {
        margin: theme.spacing(1),
        width: '100%',
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    }
}));

interface Props {
    onForecast(city: Object): void
};

const allCities = [
    {
        key: 'california',
        text: 'California'
    },
    {
        key: 'chicago',
        text: 'Chicago'
    },
    {
        key: 'new york',
        text: 'New York'
    }
];

const Form: React.FC<Props> = ({ onForecast }) => {
    const classes = useStyles();
    const defaultCity = { key: '', text: '' };
    const [ city, setCity ] = useState<{ key: string, text: string }>(defaultCity);

    const onCityChange = (e: React.ChangeEvent<any>) => {
        const selected = e.target.value;
        const selectedCity: any = allCities.filter(i => i.key === selected)[0] || defaultCity;
        setCity(selectedCity);
    };

    const onGetForecast = () => {
        if (city.key) {
            onForecast({...city});
        }
    };

    return (
        <>
            <Grid item xs={1}></Grid>
            <Grid item xs={10}>
                <Paper className={classes.paperCenter}>
                    <Grid container spacing={2}>
                        <Grid item xs={4}></Grid>
                        <Grid item xs={4}>
                            <FormControl className={classes.formControl}>
                                <InputLabel id="demo-simple-select-label">Select City</InputLabel>
                                <Select
                                    labelId="demo-simple-select-label"
                                    value={city.key}
                                    onChange={e => onCityChange(e)}
                                    fullWidth
                                >
                                    {
                                        allCities.map(
                                            i => <MenuItem value={i.key} key={i.key}>{i.text}</MenuItem>
                                        )
                                    }
                                </Select>
                            </FormControl>
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                className={classes.submit}
                                onClick={onGetForecast}
                            >
                                Get Forecast
                            </Button>
                        </Grid>
                        <Grid item xs={4}></Grid>
                    </Grid>
                </Paper>
            </Grid>
            <Grid item xs={1}></Grid>
        </>
    );
};

export default Form;