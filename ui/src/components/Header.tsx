import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
    Typography,
    Paper,
    Grid,
} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    paperCenter: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    }
}));

const Header: React.FC = () => {
    const classes = useStyles();
    return (
        <>
            <Grid item xs={1}></Grid>
            <Grid item xs={10}>
                <Paper className={classes.paperCenter}>
                    <Typography component="h1" variant="h4">
                        eJam Weather App
                    </Typography>
                </Paper>
            </Grid>
            <Grid item xs={1}></Grid>
        </>
    );
};

export default Header