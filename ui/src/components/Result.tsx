import React, { useState, useEffect, createRef } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
    Typography,
    Paper,
    Grid,
    FormControlLabel,
    Switch,
    List,
    ListItem,
    Divider
} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
        padding: theme.spacing(2)        
    },
    paper: {
        padding: theme.spacing(2),
        color: theme.palette.text.secondary,
    },
    paperCenter: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    textCenter: {
        textAlign: 'center',
    },
    textRight: {
        textAlign: 'right',
    },
}));

interface Props {
    city: any;
    forecast: any[];
    isError: boolean;
};

const getDay = (index: number) => {
    const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    return days[index];
}

const getMonth = (index: number) => {
    const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    return months[index];
};

const getDateString = (seconds: number) => {
    const date = new Date(seconds * 1000);
    return `${getDay(date.getDay())}, ${getMonth(date.getMonth())} ${date.getDate()} ${date.getFullYear()}`;
}

const getCurrentTemp = (temps: any) => {
    const hours = new Date(Date.now()).getHours();
    let tempNow = 0;
    switch (true) {
        case (hours >= 6 && hours < 12):
            tempNow = Math.round(temps.morn);
        break;

        case (hours >= 12 && hours < 18):
            tempNow = Math.round(temps.day);
        break;

        case (hours >= 18 && hours < 24):
            tempNow = Math.round(temps.eve);
        break;

        default:
            tempNow = Math.round(temps.night);
        break;
    }
    return tempNow;
}

const getAvgTemp = (temp: any): number => {
    const tempArr = Object.keys(temp).map(key => temp[key]);
    return Math.round(tempArr.reduce((sum, current) => sum + current) / tempArr.length);
}

const isDay = ():boolean => {
    const hours = new Date(Date.now()).getHours();
    return hours >= 7 && hours <= 20;
}

const Result: React.FC<Props> = ({ city, forecast, isError }) => {
    const classes = useStyles();
    const [ tempUnit, setTempUnit ] = useState<string>('C');
    const foreCastListRef = createRef<HTMLDivElement>();

    useEffect(() => {
        setTimeout(() => {
            foreCastListRef.current && foreCastListRef.current.scrollIntoView({ behavior: "smooth" });
        }, 200);
    }, [forecast.length]);
    
    const onUnitChange = (): void => {
        setTempUnit(tempUnit === 'C' ? 'F' : 'C');
    }

    const getTempByUnit = (c: number): number => {
        return Math.round(tempUnit === 'C' ? c : ((c * (9 / 5)) + 32))
    }

    const displayMessage = (isError: boolean) => <Paper className={classes.paperCenter}>
        <Typography component="h1" variant="h5">
            { isError ? 'Oops! Something went wrong' : 'Select City to view Forecast'}
        </Typography>
    </Paper>;

    const today = forecast[0];

    return (
        <>
            <Grid item xs={1}></Grid>
            <Grid item xs={10}>
                {
                    !forecast.length ? displayMessage(isError) : <Paper className={classes.paper}>
                        <Grid container spacing={2}>
                            <Grid item xs={4} md={2}>
                                <Typography component="h1" variant="h5">
                                    {city.name}
                                </Typography>
                            </Grid>
                            <Grid item xs={7} md={7} className={`${classes.textCenter}`}>
                                <Typography component="h1" variant="h5" className="today-date">
                                    {getDateString(today.dt)}
                                </Typography>
                                <Typography component="h1" variant="h6" className="today-date text-cap">
                                    {today.weather[0].description}
                                </Typography>
                            </Grid>
                            <Grid item xs={12} md={3} className={`${classes.textRight} switch-wrap`}>
                                <FormControlLabel
                                    control={
                                        <Switch
                                            checked={tempUnit === 'F'}
                                            onChange={onUnitChange}
                                            name="checkedB"
                                            color="primary"
                                        />
                                    }
                                    label={`Change To °${tempUnit === 'C' ? 'F' : 'C'}`}
                                    labelPlacement="start"
                                />
                            </Grid>
                        </Grid>
                        <Grid container spacing={2} className="today-temp-wrap">
                            <Grid item xs={12} md={6} className={classes.textCenter}>
                                <Typography component="h1" variant="h1" className="today-temp">
                                    {`${getTempByUnit(getCurrentTemp(today.temp))}°${tempUnit} `}
                                    <i className={`wi wi-owm-${isDay() ? 'day' : 'night'}-${today.weather[0].id} main-ico`} />
                                </Typography>
                            </Grid>
                            <Grid item xs={6} md={3}>
                                <List className={classes.root}>
                                    <ListItem>
                                        <b>Morning {`${getTempByUnit(today.temp.morn)}°${tempUnit}`}</b>
                                    </ListItem>
                                    <ListItem>
                                        <b>Day {`${getTempByUnit(today.temp.day)}°${tempUnit}`}</b>
                                    </ListItem>
                                </List>
                            </Grid>
                            <Grid item xs={6} md={3}>
                                <List className={classes.root}>
                                    <ListItem>
                                       <b>Evening {`${getTempByUnit(today.temp.eve)}°${tempUnit}`}</b>
                                    </ListItem>
                                    <ListItem>
                                        <b>Night {`${getTempByUnit(today.temp.night)}°${tempUnit}`}</b>
                                    </ListItem>
                                </List>
                            </Grid>
                        </Grid>
                        <div ref={foreCastListRef}>
                            <Grid container spacing={2}>
                                {forecast.slice(1).map(day => (
                                    <Grid item xs={4} md={2} key={day.dt} className={classes.textCenter}>
                                        <Typography component="h1" variant="h6">{getDay(new Date(day.dt * 1000).getDay())}</Typography>
                                        <i className={`wi wi-owm-${day.weather[0].id} wi-forecast`}></i>
                                        <p><b>{`${getTempByUnit(getAvgTemp(day.temp))}°${tempUnit}`}</b></p>
                                    </Grid>
                                ))}
                            </Grid>
                        </div>
                    </Paper>
                }
            </Grid>
            <Grid item xs={1}></Grid>
        </>
    );
};

export default Result;