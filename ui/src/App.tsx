import React from 'react';
import './App.css';
import { Provider } from 'react-redux';
import store from './store';

import WeatherApp from './containers/WeatherApp';

const App: React.FC = () => {
  return (
    <Provider store={store}>
      <WeatherApp>
      </WeatherApp>
    </Provider>
  );
};

export default App;
