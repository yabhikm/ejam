const initialState = {
    id: null,
    name: null
};

export const city = (state: any = initialState, action: any) => {
    const { payload = {} } = action;
    switch(action.type) {
        case 'FORECAST_LOADING':
        case 'FORECAST_FAIL':
            state = initialState;
        break;

        case 'FORECAST_SUCCESS':
            state = {
                ...payload.city
            };
        break;

        default:
        break;
    }
    return state;
};