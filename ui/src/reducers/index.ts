import { forecast } from './forecast';
import { city } from './city';
import { app } from './app';

export default {
    forecast,
    city,
    app
};