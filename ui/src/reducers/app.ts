const initialState = {
    isLoading: false,
    isError: false,
};

export const app = (state: any = initialState, action: any) => {
    switch(action.type) {
        case 'FORECAST_SUCCESS':
            state = {
                isLoading: false,
                isError: false
            };
        break;

        case 'FORECAST_FAIL':
            state = {
                isLoading: false,
                isError: true
            };
        break;

        case 'FORECAST_LOADING':
            state = {
                isLoading: true
            };
        break;

        default:
        break;
    }
    return state;
};