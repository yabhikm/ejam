const initialState: any[] = [];

export const forecast = (state: any = initialState, action: any) => {
    const { payload = {} } = action;
    switch(action.type) {
        case 'FORECAST_LOADING':
        case 'FORECAST_FAIL':
            state = initialState;
        break;

        case 'FORECAST_SUCCESS':
            state = [
                ...payload.forecast
            ];
        break;

        default:
        break;
    }
    return state;
};