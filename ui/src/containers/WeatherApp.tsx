import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk'

import { getForeCast } from '../actions';

import Header from '../components/Header';
import Form from '../components/Form';
import Result from '../components/Result';
import Loader from '../components/Loader';

interface Props {
    dispatch: ThunkDispatch<any, any, any>,
    city: any,
    forecast: any[],
    isLoading: boolean,
    isError: boolean
};

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
        padding: theme.spacing(2)
    },
    paperCenter: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
}));

const WeatherApp: React.FC<Props> = ({ dispatch, city, forecast, isLoading, isError }) => {
    const classes = useStyles();

    const onForecast = (selectedCity: any) => {
        dispatch(getForeCast(selectedCity.key));
    };

    return (
        <div className={classes.root}>
            <Grid container spacing={2}>
                <Header></Header>
                <Form
                    onForecast={onForecast}
                ></Form>
                {
                    isLoading ?
                    <Loader /> :
                    <Result
                        city={city}
                        forecast={forecast}
                        isError={isError}
                    >
                    </Result>
                }
            </Grid>
        </div>
    );
};

interface LinkStateProps {
    forecast: any[],
    city: any,
    isLoading: boolean,
    isError: boolean
};

interface LinkDispatchProps {
    dispatch: ThunkDispatch<any, any, any>
};

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, any>): LinkDispatchProps => ({
    dispatch
});

const mapStateToProps = (state: any): LinkStateProps => ({
    forecast: state.forecast || [],
    city: state.city || {},
    isLoading: state.app.isLoading,
    isError: state.app.isError
});

export default connect(mapStateToProps, mapDispatchToProps)(WeatherApp);